'use strict';
const route = require('express').Router();
const userController = require('../controllers/userController');

route.get('/', userController());

module.exports = route;