'use strict';
const log           = require('morgan');
const cors          = require('cors');
const path          = require('path');
const express       = require('express');
const session       = require('express-session');
const bodyParser    = require('body-parser');


const app           = express();
app.set('port', 9090);
app.use(cors());
app.use(log());
app.use(session());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.listen(app.get('port'), () => {
    console.log('1');
});

